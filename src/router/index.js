import Vue from 'vue'
import Router from 'vue-router'
import Bugs from '@/components/pages/bugs.client.component.vue'
import Home from '@/components/pages/home.client.component.vue'
import Devices from '@/components/pages/devices.client.component.vue'
import Testers from '@/components/pages/tester.client.component.vue'
var log =console.log;

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    }, 
    {
      path: '/bugs', 
      name: 'bugs', 
      component: Bugs,
    },
    {
      path:'/devices',
      name: 'devices',
      component: Devices
    },
    {
      path:'/testers',
      name: 'testers',
      component: Testers
    }
  ]
})
