//Module dependencies
import axios from 'axios'
// initial state
const state = {
	queryCountriesArray: [],// countries provied to use by the dropdown
	queryDevicesArray: [],// devices provied to us by the dorpdown
	deviceUserRelationArray: [], // an array of our users with matched Devices attached
	calculatedBugArrayForDevicByUser : [],
	testers: {
		testersArray: [],
		testerDevices: [],
		queryTesters: [],
		countryMatchedTesters: [],
	},
	devices: {
		devicesArray: [],
		queryMatchedDevices: [],
	},
	bugs: {
		bugsLength: '',
		bugsArray: []
	}
}

/*
* allCheck 
* reusable helper method 
* searches an array for All 
* returns true or false;
*/
var allCheck = function(someArray){
	var allFlag = false;
	// determin criteriaDevicesArray	
	for(var x= 0; x < someArray.length; x ++){
		// check for all 
		if(someArray[x].data === 'ALL'){
			allFlag = true;
			// we can stop the array
			break;
		}
	}
	return allFlag
};// end allCheck

/*
* arrayOne should be the larger array
* checks to se if there is a match in the two arrays 
* it then pushes that match into a new object. 
* returns the matches array
*/
var matchCheck = function(arrayOne, arrayTwo){
	var matchedObjects = [];

	// loop over the provided array One
	for(var x = 0; x < arrayOne.length; x ++){
		// loop over provided arrayTwo
		inner: for(var y = 0; y < arrayTwo.length; y ++){
			// check for match 
			if(arrayOne[x].description === arrayTwo[y].data){
				// push matches into matchedDevices
				matchedObjects.push(arrayOne[x]);
				// end this iteration we found our match
				break inner;
			}
		}
	}	
	return matchedObjects;

};


const getters = {

	// returns the bugCountByDevice in ascending order based off of bug count
	getBugCountByDeviceArrayAscending: (state) =>{
		// //check if we have data 
		if(!state.calculatedBugArrayForDevicByUser.length){
			return 'working on it '
		}

		var totalsArray = [];
		// loop over all of the calculated values
		for(var x = 0; x < state.calculatedBugArrayForDevicByUser.length; x ++){
			var obj = {};
			obj.total = 0;
			// helper function to get the devices name 
			var getDeviceName = function(deviceId){
				// loop over devices 
				for(var x= 0; x < state.devices.devicesArray.length; x ++){
					// check for match
					if(deviceId === state.devices.devicesArray[x].deviceId){
						return state.devices.devicesArray[x].description;
					}
				}
			};
			// helper function to get the users name 
			var getTesterName = function(testerId){
				// loop over devices 
				for(var x= 0; x < state.testers.testersArray.length; x ++){
					// check for match
					if(testerId === state.testers.testersArray[x].testerId){
						return state.testers.testersArray[x].firstName + ' ' + state.testers.testersArray[x].lastName;
					}
				}				
			}

			// loop over all the testers for tester 1
			for(var y = 0; y < state.calculatedBugArrayForDevicByUser.length; y ++ ){
				// only show devices for tester 1 
				// if(state.calculatedBugArrayForDevicByUser[y].testerId == '2'){
				if(state.calculatedBugArrayForDevicByUser[y].testerId === state.calculatedBugArrayForDevicByUser[x].testerId){
					if(state.calculatedBugArrayForDevicByUser[y].deviceId === state.calculatedBugArrayForDevicByUser[x].deviceId){

						obj.deviceName = getDeviceName(state.calculatedBugArrayForDevicByUser[x].deviceId);
						obj.testerName = getTesterName(state.calculatedBugArrayForDevicByUser[y].testerId);
						obj.testerId = state.calculatedBugArrayForDevicByUser[y].testerId 
						obj.deviceId = state.calculatedBugArrayForDevicByUser[x].deviceId;
						// increment count 
						obj.total += state.calculatedBugArrayForDevicByUser[y].count;
					}// inner if
				}// outer if

			}//inner for 

			totalsArray.push(obj);
		}// end first loop 

		var finishedArray = _.orderBy(totalsArray, ['total'], ['desc']);

		return finishedArray;
	},// end getBugCountByDeviceArrayAscending

	// returns the length of the array
	getDeviceUserRelationArrayCount: ( state) =>{
		// check if we have data 
		if(!state.deviceUserRelationArray.length){
			return 'working on it';
		}
		return state.deviceUserRelationArray.length;
	},
	// returns deviceUserRelationsArray
	getDeviceUserRelationArray: ( state)=>{
		if(!state.deviceUserRelationArray){
			return 'working on it';
		}
		return state.deviceUserRelationArray;
	},

	/*
	* getDevicesDescriptionArrayAlphabetical
	* this function loops over the devices array and creates a new array with 
	* description in alphabetical order
	* then it pushes all of the descriptions into a new arrayand returns that
	* it then creates a last option ALL
	*/
	getDevicesDescriptionArrayAlphabetical: ( state) => {
		// check if devices exist
		if(!state.devices.devicesArray.length){
			return false;
		}

		var devicesArray = [];
		
		// sort over the array and arrange the devices array in alphabetical order
		var freshArray = _.sortBy(state.devices.devicesArray, function(i){
			return i.description.toLowerCase()
		});

		// loop over our fresh arary and push values into devices ARray
		for(var x =0; x < freshArray.length;x++){
			var obj = {};
			obj.data =freshArray[x].description;
			devicesArray.push(obj);
		}

		var all = {'data': 'ALL'}
		// Then add all to the front of the array
		devicesArray.unshift(all);
		return devicesArray;
	},

	/*
	* getCountires
	* this function loops over the testers.testersArray 
	* extracging every country into a new country array
	* then we use lodash to remove dupes. 
	* returns clean duplicate free array 
	*/
	getCountries: (state) => {
		// if there are no teters 
		if(!state.testers.testersArray.length){
			return false;
		}

		var countryArray = [];

		// loop over all of the testers and pull the countries out of the array 
		for(var x = 0; x < state.testers.testersArray.length; x ++){
			// create objet
			var object = {};
			object.data = state.testers.testersArray[x].country
			// push all the countries into the pais array
			countryArray.push(object);
		}

		// lodash remove duplicates
		var freshArray =  _.uniqBy(countryArray, function (data) {
		  return data.data
		});

		// create al object
		var all = {'data': 'ALL'}
		// ad all to the aray
		freshArray.unshift(all);
		return freshArray;
	},


	// get testers Array
	getTestersArray: (state) =>{
		return state.testers.testersArray;
	},

	// get devices Array
	getDevicesArray: (state) =>{
		return state.devices.devicesArray;
	},
	// get bugs length
	getBugsLengh: (state) => {
		return state.bugs.bugsLength;
	},
	// get bugs arary 
	getBugsArray: (state) =>{
		return state.bugs.bugsArray;
	}, 
};

const actions = {

	// loops through deivces gets device by deviceid
	getDeviceNameById({cmmmit, state}, deviceId){

		return new Promise((resolve, reject) => {
			// loop over devices 
			for(var x= 0; x < state.devices.devicesArray.length; x ++){
				// check for match
				if(deviceId === state.devices.devicesArray[x].deviceId){
					resolve(state.devices.devicesArray[x].description);
				}
			}
		});
	},// end get device name by id 

	/*
	* calculateBugCountForDeviceByUser
	* first this loops over country filtered usrs
	* puts them into a ne array 
	* then it loops over the new array to find the bug count 
	* seperates bug count, bug id, user id, device id into its own object
	* then it throws all of those into an array 
	* commits the changes of that array 
	*/
	calculateBugCountForDeviceByUser({commit, state}){
		return new Promise((resolve, reject) => {

			var matchedDevicesArray = [];
			// loop over all of the countrymatched tsters so that we can get access to the matched devices array 
			for(var x = 0; x < state.testers.countryMatchedTesters.length; x ++){
				// loop over each tsters matchd data so that we can set data and init some props
				for(var y = 0; y < state.testers.countryMatchedTesters[x].matchedDevices.length; y ++){
					// add teseter Data to the object 
					state.testers.countryMatchedTesters[x].matchedDevices[y].testerData = state.testers.countryMatchedTesters[x];
					// ini a bugs array on all ierations
					state.testers.countryMatchedTesters[x].matchedDevices[y].bugs = [];
					state.testers.countryMatchedTesters[x].matchedDevices[y].bugCountByDevice = []
					matchedDevicesArray.push(state.testers.countryMatchedTesters[x].matchedDevices[y]);

				}
			}
			// init bug object for loop interaction
			var counter = 0;
			var bugObj = {};
			var bugsArray = [];

			// loop over matched devices  so that we can compare them against the bugs
			for(var x = 0; x < matchedDevicesArray.length; x ++){
				// loop over all the bugs so we can check for matches 
				for(var y = 0; y < state.bugs.bugsArray.length; y ++){
					// check if bug tester Id matches matchedDevices device Id	
					if(matchedDevicesArray[x].testerId === state.bugs.bugsArray[y].testerId){
						// now check if bug.Device id matches matchedDevicesArray.device id
						if(matchedDevicesArray[x].deviceId === state.bugs.bugsArray[y].deviceId){
							// increment the counter because we just had another match
							counter ++;
							bugObj.count = counter; 
							bugObj.deviceId = matchedDevicesArray[x].deviceId;
							bugObj.testerId = matchedDevicesArray[x].testerId;
							matchedDevicesArray[x].bugs.push(state.bugs.bugsArray[y]);
						}//end matched did = bugs did 
					}
					// only run this if we'r on the last iteration of the loop 
					if((state.bugs.bugsArray.length -1 ) === y){
						// only save this if the count has data
						if(bugObj.count > 1){
							// WE GOT GOLD BABY! 
							//if this is the finaliteration store the data
							// matchedDevicesArray[x].bugCountByDevice.push(bugObj);
							bugsArray.push(bugObj);
						}
					}
				}

				// reset the object for the next iter
				bugObj = {};
				bugObj.count = 0;
				counter = 0;	
			}// END FOR LOOP 

			commit('setCalculatedBugArrayForDevicByUser', bugsArray);
			resolve();
		});// end promise 
	},//end calculate bugCountForDevices

	/*
	* calculateTesterDeviceRelation
	*  first it initializes some props on countrymatched testers
	* Then it loops over filtered testers 
	* if the tester has a dvice that matches with the devices array it
	* it creates a new device user relation boject and throws that into an array 
	* then it commits that arary
	*/
	calculateTesterDeviceRelation({commit, state}){
		return new Promise((resolve, reject) => {

			var relationArray = [];
			// loop over all the testesrs and add a matchedDevices 
			for(var x = 0; x < state.testers.countryMatchedTesters.length; x ++ ){
				state.testers.countryMatchedTesters[x].matchedDevices = [];
			}
			// loover all of the provided devices
			for (var x =0; x < state.devices.queryMatchedDevices.length; x ++){
				// loop over all of the test Deivce aray
				for(var y = 0; y < state.testers.testerDevices.length; y ++){
					// check if there is a device that matches
					if(state.devices.queryMatchedDevices[x].deviceId === state.testers.testerDevices[y].deviceId){

						// loop over provided users and check if matched device matches a user 
						for(var z = 0; z < state.testers.countryMatchedTesters.length; z ++){
							// check if tester matches matched device
							if(state.testers.countryMatchedTesters[z].testerId === state.testers.testerDevices[y].testerId){
								state.testers.countryMatchedTesters[z].matchedDevices.push(state.testers.testerDevices[y]);
							}// end inner if
						}// end inner2 for 
					}// end outer if 
				}// end inner 1 for
			}// end for

			// loop over all of our results andpush oly the ones that have data
			for(var x = 0; x < state.testers.countryMatchedTesters.length; x ++){
				// check that our matched testers have data in matched devices befor pushing
				if(state.testers.countryMatchedTesters[x].matchedDevices.length){
					// push data into array
					commit('setDeviceUserRelationArray', state.testers.countryMatchedTesters[x]);				
				}

			}			
			resolve(state.testers.countryMatchedTesters);
		});//end promise 
	},// end calculateTesterDevice Relation

	/*
	* calculateQueryMatches
	* takes in an array of devices
	* calls the AllCheck method 
	* calls the matchCheck mthod 
	* returns the result of either Allcheck or Match Check
	*/
	calculateQueryMatches({commit, state}){
		return new Promise((resolve, reject) => {
			var criteriaDevicesArray = [];
			// determin criteriaDevicesArray
			var allFlag = allCheck(state.queryDevicesArray);
			var matchedDevices =  [];
			if(allFlag){
				matchedDevices =  state.devices.devicesArray;
			}else{
				var matchedDevices = matchCheck(state.devices.devicesArray, state.queryDevicesArray);
			}
			commit('setQuerymatchedDevices',matchedDevices )
			resolve(matchedDevices);
		});
	},


	/*
	* calculateTestersByCountry
	* filters out users by country provided. 
	* returns that array 
	*/
	calculateTestersByCountry({commit, state}){
		return new Promise((resolve, reject) => {
			// get users by country	
			var countryAllFlag = allCheck(state.queryCountriesArray);
			var countryMatchedTesters = [];
			// check if all was selected
			if(countryAllFlag){
				// get all users
				countryMatchedTesters = state.testers.testersArray;
			}else{
				// loop over all of the testers
				for(var x = 0; x < state.testers.testersArray.length; x ++){
					var testerCountryMatchFlag = false;
					// check if tester has one of the following countries 
					for(var y = 0; y < state.queryCountriesArray.length; y ++ ){
						// checking if user matches country state.queryCountriesArray
						if(state.queryCountriesArray[y].data === state.testers.testersArray[x].country){
							// pushin matched users into array
							countryMatchedTesters.push(state.testers.testersArray[x]);
						}
					}
				}
			}
			// commit changes
			commit('setCountryMatchedTesters', countryMatchedTesters);
			resolve(countryMatchedTesters);
			return;
		});
	},
};

const mutations = {
	// store setCalculatedBugArrayForDevicByUser setter
	setCalculatedBugArrayForDevicByUser(state, providedArray){
		state.calculatedBugArrayForDevicByUser = providedArray;
	},
	// sets the store back to emtpy array
	zeroOut(state){
		state.deviceUserRelationArray = [];
		state.devices.queryMatchedDevices= [];
		state.devices.queryMatchedDevices = [];
		state.testers.countryMatchedTesters =[];
		// state.queryDevicesArray =[];
		return;
	},
	//store setDevicRelationArray settr
	setDeviceUserRelationArray(state, providedArray){
		state.deviceUserRelationArray.push(providedArray);
	},
	// state queryMatchedDevices setter
	setQuerymatchedDevices ( state, providedArray){
		state.devices.queryMatchedDevices = providedArray;
	},
	//state countrymatchedTeters setter
	setCountryMatchedTesters ( state, providedArray){
		state.testers.countryMatchedTesters = providedArray;
	},
	// state queryCountries stter
	setQueryCountries ( state, providedArray){
		state.queryCountriesArray = providedArray;
	},
	//state queryDevices setter
	setQueryDevices ( state, providedArray){
		state.queryDevicesArray = providedArray;
	},
	// state testrDevices stter
	setTesterDevices ( state, testerDevicesArray){
		state.testers.testerDevices = testerDevicesArray;
	},
	// state tester setter
	setTester ( state, testersArray){
		state.testers.testersArray = testersArray;
		return;
	},
	// state devices setter
	setDevices ( state, devicesArray){
		state.devices.devicesArray = devicesArray;
		return;
	},
	// state bugs setter
	setBugs ( state, bugArray){
		state.bugs.bugsArray =  bugArray;
		state.bugs.bugsLength = bugArray.length;
		return
	}
};

export default {
  state,
  getters,
  actions,
  mutations
}