//Module dependencies
import axios from 'axios'
var log = console.log;

// initial state
const state = {
	baseUrl : process.env.BASE_URL,
}
const actions = {

	/*
	* GetTesters
	* Sends a qury to the firebase api to load in all of our testers
	*/
	getTesters({commit, state}){
		return new Promise((resolve, reject) => {
			// send post request to the server to load in more images 
			axios.get(state.baseUrl + '/tester.json')
			.then(function(response){
				commit('setTester', response.data);
				resolve(response);
				return;
			})
			.catch(function(error){
				// something went wrong log error 
				reject(error.response);
				return;
			});
		});		
	},
	/*
	* getTesterDevices
	* sends a query to the firebase api to load in all testerDevics
	*/
	getTesterDevices({commit, state}){
		return new Promise((resolve, reject) => {
			// send post request to the server to load in more images 
			axios.get(state.baseUrl + '/tester_device.json')
			.then(function(response){
				commit('setTesterDevices', response.data);
				resolve(response);
				return;
			})
			.catch(function(error){
				// something went wrong log error 
				reject(error.response);
				return;
			});
		});	
	},// end getTesterDevices
	/*
	* GetDevices
	* Sends a qury to the firebase api to load in all of our devices
	*/
	getDevices({commit, state}){
		return new Promise((resolve, reject) => {
			// send post request to the server to load in more images 
			axios.get(state.baseUrl + '/device.json')
			.then(function(response){
				commit('setDevices', response.data);
				resolve(response);
				return;
			})
			.catch(function(error){
				// something went wrong log error 
				reject(error.response);
				return;
			});
		});		
	},
	/*
	* GetBugs
	* Sends a qury to the firebase api to load in all of ourbugs
	*/
	getBugs({commit, state}){
		return new Promise((resolve, reject) => {
			// send post request to the server to load in more images 
			axios.get(state.baseUrl + '/bugs.json')
			.then(function(response){
				commit('setBugs', response.data);
				resolve(response);
				return;
			})
			.catch(function(error){
				// something went wrong log error 
				reject(error.response);
				return;
			});
		});		
	}, 
};

export default {
  state,
  actions,
}