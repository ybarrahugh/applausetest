import Vue from 'vue'
import Vuex from 'vuex'
import API from './modules/api'
import QueryLogic from './modules/queryLogic'


Vue.use(Vuex)


export default new Vuex.Store({
	// Strict mode should only run in development mode turns off in production
  	strict: process.env.NODE_ENV !== 'production',
    modules: {
    	API,
    	QueryLogic
    },
})