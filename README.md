# applausfire

> simple sorting app 

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


Hey guys I just wanted to say thank you for taking the time to work with me, review my code and interview with me. 
I just wanted to touch a little bit on what I did here. 

 

The project consists of a a single page application and a remote databas where I uploaded the csvs

Here is a list of everything I used in the project 

Firebase for the databae I ended up converting all the .csv's to json and uploaded them there

[https://firebase.google.com/](https://firebase.google.com/)

I used Vue js for the front end applciation framework 

[https://vuejs.org/](https://vuejs.org/)

I used _ lodash for a copule of their helper methods 

[https://lodash.com/](https://lodash.com/)

I used vue-multiselect beeacuse I got stuck trying to get the multiselect done on my own. ((life saver))
[https://monterail.github.io/vue-multiselect/](https://monterail.github.io/vue-multiselect/)